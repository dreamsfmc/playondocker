# Ejecucion de un script PHP desde un container

En este directorio tenemos 3 ficheros

- megaScript.php => El script en PHP en cuestion
- start.sh
- readme.md

1. Lo primero que vamos a hacer es arrancar un container PHP por linea de comandos pasandole el fichero a ejecutar

```bash
docker run -it --rm --name megaScript -v $(pwd):/usr/src/myapp -w /usr/src/myapp php php megaScript.php
```

2. Pero como yo soy muy vago, me creo un bash script para no introducir esa pedazo de linea a mano

```bash
./start.sh megaScript.php
bash start.sh megaScript.php

# Nota: El script puede no tener extensión (./start megaScript.php o bash start megaScript.php)
```

3. Pero esto sigue siendo una patata si quiero ejecutar scripts PHP desde diferentes proyectos y directorios del sistema.
Esto lo mejoramos usando este bash script desde el directorio de binarios del sistema.

```bash
cp start.sh /usr/local/bin/php

# Ahora podremos lanzar cualquier script PHP sin necesidad de instalar el PHP en nuestra maquina local.
# Si quereis lanzar una maquina especifica podriamos mejorar el script pasando el parametro de la version de PHP
```

