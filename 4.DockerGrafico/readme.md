# Dockerizar un laravel

En este directorio tenemos los siguientes ficheros

- readme.md

1. Ejecutaremos firefox dockerizado

```bash

# Primera ejecucion
docker run -d --name firefox -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix kennethkl/firefox

# Proximas ejecuciones
docker start firefox

```

2. Ejecutaremos chrome dockerizado

```bash

# Primera ejecucion
docker run -d --name chrome -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix kennethkl/chrome

# Proximas ejecuciones
docker start firefox
```