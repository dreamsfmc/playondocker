# Ejecucion de composer en un container

En este directorio tenemos 3 ficheros

- composer => script en cuestion
- readme.md

1. Lo primero que vamos a hacer es arrancar un container composer por linea de comandos pasandole el comando a ejecutar

```bash
# Podemos pasar una version de composer especifica

docker run --rm -v $(pwd):/app composer/composer -v
```

2. Pero yo sigo siendo vago y quiero algo mas intuitivo asi que vamos a ejecutar un script que sea mas facil que la linea anterior

```bash
./composer about
bash composer about
```

3. Pero aun asi lo tendria que tener por proyecto asi que lo que vamos a hacer añadirlo al directorio de binarios

```bash
cp composer /usr/local/bin/composer
composer about
```
