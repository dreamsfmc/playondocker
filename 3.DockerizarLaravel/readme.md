# Dockerizar un laravel

En este directorio tenemos los siguientes ficheros

- todos los ejercicios de laravel
- readme.md
- composer.phar
- apache

1. Lo primero que vamos a hacer es arrancar un container apache-php por linea de comandos

```bash

docker run --rm -v $(pwd):/var/www/html -p 80:80 apache-php

# Modo daemon
docker run --rm -v $(pwd):/var/www/html -p 80:80 -d apache-php

# Si falla file_get_...
Ejecutar en la maquina local los permisos adecuados como dice la documentacion de laravel:

# Si es entorno desarrollo
chmod -R 777 boostrap/cache
chmod -R 777 storage

# Si es entorno pre o pro 
chown -R 777 OWNER-APACHE-SERVER 
chown -R 777 OWNER-APACHE-SERVER 
```

2. Detener contenedores

```bash

# Detener contenedor por id
docker stop IDCONTENEDOR


# Detener el ultimo contenedor lanzado
docker stop $(docker ps | tail -n 1 | awk '{print $1}')
```

3. Sigo siendo vago y quiero algo mas molon

```bash

./apache
bash apache 

```

4. Pero yo quiero algo mas chu chu chu chuliiiiii

```bash

# Copiamos el script al directorio de binarios para lanzarlo desde cualquier carpeta
cp /usr/local/bin/apache

# Ejecutar el container con:
apache

```