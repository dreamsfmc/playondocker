# Ejecucion de un script PHP desde un container

En este directorio tenemos 3 ficheros

- 000-laravel.conf
- 000-laravel-ssl.conf
- Dockerfile
- readme.md

0. Limpiemos nuestro entorno

```bash

# Importar la imagen base
docker import apache-php.tar && docker tag $(docker images -q | head -n 1) apache-php


```

1. Compilar la imagen

```bash

docker build . -t apache-php

```