# Dockerizar un proyecto multiple

En este directorio tenemos los siguientes ficheros

- readme.md
- superweb1
- superweb2

1. Añadir los hosts necesarios
```bash

echo "127.0.0.1 superweb1.local" >> /etc/hosts
echo "127.0.0.1 superweb2.local" >> /etc/hosts

```

2. Lanzar el proxy

```bash

docker run -d -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock:ro jwilder/nginx-proxy

```

3. Lanzar los containers

```bash

# Entramos en superweb1
cd superweb1

# No especificamos puerto para que sea random
docker run --rm -e VIRTUAL_HOST=superweb1.local -v $(pwd):/var/www/html -d apache-php


# Entramos en superweb1
cd superweb2

# No especificamos puerto para que sea random
docker run --rm -e VIRTUAL_HOST=superweb2.local -v $(pwd):/var/www/html -d apache-php

```

3. Como me canso de lanzar los contenedores a mano pues un script

```bash

# Lanzar bloque de contenedores
./start

# Apagar bloque de contenedores
./stop

```
